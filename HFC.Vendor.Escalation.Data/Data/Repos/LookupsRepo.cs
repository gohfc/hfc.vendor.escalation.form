﻿using HFC.Vendor.Escalation.Data.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace HFC.Vendor.Escalation.Data
{
    public static class LookupsRepo
    {
        public static List<LookupDTO> GetProductCategoryLookup()
        {
            List<LookupDTO> lists = new List<LookupDTO>();
            using (var context = new HFCFormQueueEntities())
            {
                lists = context.Database.SqlQuery<LookupDTO>("select ID, Title from Remake_ProductCategoryLookup where BrandLookupID = 1").ToList();
            }

            return lists;
        }

        public static List<LookupDTO> GetVendorLookup()
        {
            List<LookupDTO> lists = new List<LookupDTO>();
            using (var context = new HFCFormQueueEntities())
            {
                lists = context.Database.SqlQuery<LookupDTO>("select ID, Title from Remake_VendorLookup where BrandLookupID = 1 and IsActive = 1").ToList();
            }

            return lists;
        }

    }
}
