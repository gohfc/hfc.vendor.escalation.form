﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HFC.Vendor.Escalation.Data.Data;
using System.Data.Entity.Core.Objects;

namespace HFC.Vendor.Escalation.Data
{
	public static class VendorRepo
	{
		public static bool Vendor(VendorDTO vendor)
		{
			try
			{
				if (vendor.VendorLines.Count == 0 || IfVendorExist(vendor)) return true;

				using (var context = new HFCFormQueueEntities())
				{
					//-------Insert BBVendor
					var vendorEntity = new Remake_BBVendor
					{
						IncidentDate = vendor.IncidentDate,
						OriginalOrderDate = vendor.OriginalOrderDate,
						Remake_VendorLookupID = vendor.VendorLookupID,
						OrderNumber = vendor.OrderNumber,
						FEFirstName = vendor.FirstName,
						FELastName = vendor.LastName,
						Email = vendor.Email,
						OwnerNumber = vendor.OwnerNumber,
						OwnerName= vendor.OwnerName,
						TerrName= vendor.TerrName,
						CreatedDate = DateTime.Now
					};
					
					context.Remake_BBVendor.Add(vendorEntity);
					context.SaveChanges();
					//---------------

					//---Retrieve the ID from BBVendor
					int vendorID = vendorEntity.ID;
					//------------


					foreach (var line in vendor.VendorLines)
					{
						//Insert Remake_BBVendorLine
						var lineEntity = new Remake_BBVendorLine
						{
							Remake_BBVendorID = vendorID,
							LineNumber = line.LineNumber,
							Remake_ProductCategoryLookupID = line.CategoryLookupID,
							Description = line.Description,
							IssueType = line.IssueType,
							IsRemake = line.IsRemake,
							RemakeOrderNumber = line.RemakeOrderNumber,
							IssueNotifyDate = line.IssueNotifyDate,
							OriginalShipDate = line.OriginalShipDate,
							RevisedShipDate = line.RevisedShipDate,
							IssueReason = line.IssueReason
						};
						context.Remake_BBVendorLine.Add(lineEntity);
						context.SaveChanges();
						//---------------

						//---Retrieve the ID from BBVendorLine
						int vendorLineID = lineEntity.ID;
						//------------

						foreach (var img in line.Images)
						{
							//Insert Remake_FileStream
							var imgEntity = new Remake_FileStream
							{
								stream_id = Guid.NewGuid(),
								Name = img.FileName,
								FileType = "img",// img.FileType,
								File_Stream = img.Stream,
								CreatedDate = DateTime.Now
							};
							context.Remake_FileStream.Add(imgEntity);
							context.SaveChanges();
							//---------------

							//---Retrieve the ID from Remake_FileStream
							int fileStreamID = imgEntity.ID;
							//------------

							//Insert Remake_VendorImage
							context.Remake_VendorImageInsert(vendorLineID, vendor.BrandID, fileStreamID);
						}
					}
				}
			}
			catch (Exception ex)
			{
				ex.ToString();
				return false;
			}

			return true;
		}


		private static bool IfVendorExist(VendorDTO vendor)
		{
			bool result = false;
			using (var context = new HFCFormQueueEntities())
			{
				foreach (var line in vendor.VendorLines)
				{
					var query = "SELECT count(*) FROM [dbo].[Remake_BBVendor] v join[dbo].[Remake_BBVendorLine] vl on v.ID = vl.Remake_BBVendorID " +
								"where " +
								"v.IncidentDate = '" + vendor.IncidentDate + "' and v.Remake_VendorLookupID = " + vendor.VendorLookupID + " and v.OrderNumber = '" + vendor.OrderNumber +
								"' and v.FEFirstName = '" + vendor.FirstName + "' and v.FELastName = '" + vendor.LastName + "' and v.Email = '" + vendor.Email +
                                "' and v.OwnerName = '" + vendor.OwnerName + "' and v.TerrName = '" + vendor.TerrName +
                                "' and v.OwnerNumber = '" + vendor.OwnerNumber + "' and v.OriginalOrderDate = '" + vendor.OriginalOrderDate + "' and vl.Description = '" + line.Description +
								"' and vl.IsRemake = " + (line.IsRemake == true ? 1 : 0) + " and vl.IssueReason = '" + line.IssueReason +
								"' and vl.IssueType = '" + line.IssueType + "' and vl.LineNumber = '" + line.LineNumber + "' and vl.Remake_ProductCategoryLookupID = " + line.CategoryLookupID +
								" and vl.RemakeOrderNumber = '" + line.RemakeOrderNumber + "'";

					result = (context.Database.SqlQuery<int>(query).First() >= 1) ? true : false;

					if (result) return result;
				}
			}

			return result;
		}
	}
}
