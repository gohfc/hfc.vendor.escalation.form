﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HFC.Vendor.Escalation.Data
{
    public class VendorDTO
    {
        public DateTime? IncidentDate { get; set; }
        public DateTime? OriginalOrderDate { get; set; }
        public Int32 VendorLookupID { get; set; }
        public String OrderNumber { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Email { get; set; }
        public String OwnerNumber { get; set; }
        public String OwnerName { get; set; }
        public String TerrName { get; set; }
        public Byte BrandID { get; set; }
        public List<VendorLine> VendorLines { get; set; }
    }

    public class VendorLine
    {
        public String LineNumber { get; set; }
        public Int32 CategoryLookupID { get; set; }
        public String Description { get; set; }
        public String IssueType { get; set; }
        public bool IsRemake { get; set; }
        public String RemakeOrderNumber { get; set; }
        public DateTime? IssueNotifyDate { get; set; }
        public DateTime? OriginalShipDate { get; set; }
        public DateTime? RevisedShipDate { get; set; }
        public String IssueReason { get; set; }
        public List<FileStream> Images { get; set; }
    }
    public class FileStream
    {
        public String VLine { get; set; }
        public String FileName { get; set; }
        public String FileType { get; set; }
        public byte[] Stream { get; set; }
    }
}
