﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HFC.Vendor.Escalation.Data
{
    public class LookupDTO
    {
        public int ID { get; set; }
        public string Title { get; set; }
    }
}
