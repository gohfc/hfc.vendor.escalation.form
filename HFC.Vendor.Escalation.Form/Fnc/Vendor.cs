﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace HFC.Vendor.Escalation.Form.Fnc
{
    public static class Vendor
    {
        internal static bool ValidateVendorLine(Models.VendorLine data)
        {
            return (data != null &&
                !string.IsNullOrEmpty(data.Description) &&
                !string.IsNullOrEmpty(data.IsRemake) &&
                data.IssueNotifyDate != default(DateTime) &&
                !string.IsNullOrEmpty(data.IssueReason) &&
                !string.IsNullOrEmpty(data.IssueType) &&
                !string.IsNullOrEmpty(data.LineNumber) &&
                //!string.IsNullOrEmpty(data.ProductCategory) &&
                //!string.IsNullOrEmpty(data.ProductCategoryId) &&
                !string.IsNullOrEmpty(data.RemakeOrderNumber));
        }

        internal static List<Models.FileStream> ReadImages(HttpFileCollectionBase hfc)
        {
            List<Models.FileStream> files = new List<Models.FileStream>();
            try
            {
                for (int i = 0; i < hfc.Count; i++)
                {
                    HttpPostedFileBase hpf = hfc[i];
                    var filestream = new Models.FileStream();

                    if (hpf.ContentLength > 0)
                    {
                        filestream.FileName = Path.GetFileName(hpf.FileName);
                        filestream.FileType = hpf.ContentType;

                        using (Stream fs = hpf.InputStream)
                        {
                            using (BinaryReader br = new BinaryReader(fs))
                            {
                                filestream.Stream = br.ReadBytes((Int32)fs.Length);
                            }
                        }
                    }
                    else
                        return null;

                    files.Add(filestream);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return files;
        }

        internal static Models.VendorLine CopyVendorLine(List<Models.VendorLine> vendorLines, string id)
        {
            var copy = vendorLines.Find(l => l.LineNumber == id);
            return copy;
        }

        internal static List<Models.VendorLine> DeleteVendorLine(List<Models.VendorLine> vendorLines, string id)
        {
            vendorLines.Remove(vendorLines.Find(l => l.LineNumber == id));
            return vendorLines;
        }
    }
}