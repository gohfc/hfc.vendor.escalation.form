﻿using System.Web;
using System.Web.Mvc;

namespace HFC.Vendor.Escalation.Form
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}