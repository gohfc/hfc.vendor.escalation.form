﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HFC.Vendor.Escalation.Data;
using System.Web.Mvc;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Configuration;


namespace HFC.Vendor.Escalation.Form.Services
{
    public class VendorService : IVendorService
    {
        public bool Vendor(Models.Vendor model)
        {
            return VendorRepo.Vendor(ConvertToDTO(model));
        }

        private VendorDTO ConvertToDTO(Models.Vendor model)
        {
            VendorDTO result = new VendorDTO
            {
                BrandID = 1,
                Email = model.EmailAddress,
                FirstName = model.FirstName,
                LastName = model.LastName,
                OwnerNumber = model.OwnerNumber,
                OwnerName = model.OwnerName,
                TerrName = model.TerrName,
                OriginalOrderDate = model.OroginalOrderDate,
                IncidentDate = model.IncidentDate,
                VendorLookupID = model.LockedVendorId,
                OrderNumber = model.LockedOrderNumber,
                VendorLines = ConvertVendorLines(model.VendorLines)
            };
            return result;
        }

        private List<VendorLine> ConvertVendorLines(List<Models.VendorLine> list)
        {
            List<VendorLine> results = new List<VendorLine>();
            if (list != null && list.Count > 0)
                foreach (var item in list)
                {
                    VendorLine result = new VendorLine()
                    {
                        LineNumber = item.LineNumber,
                        CategoryLookupID = item.ProductCategoryId,
                        Description = item.Description,
                        IsRemake = item.IsRemake == "Yes" ? true : false,
                        IssueType = item.IssueType,
                        IssueNotifyDate = item.IssueNotifyDate,
                        OriginalShipDate = item.OrigEstShipDate,
                        RevisedShipDate = item.RevEstShipDate,
                        IssueReason = item.IssueReason,
                        Images = ConvertImages(item.Images)
                    };
                    results.Add(result);
                }

            return results;
        }

        private List<Data.FileStream> ConvertImages(List<Models.FileStream> list)
        {
            List<Data.FileStream> results = new List<Data.FileStream>();
            if (list != null && list.Count > 0)
                foreach (var item in list)
                {
                    Data.FileStream result = new Data.FileStream()
                    {
                        FileName = item.FileName,
                        FileType = item.FileType,
                        Stream = item.Stream,
                        VLine = item.VLine
                    };
                    results.Add(result);
                }

            return results;
        }

        public List<SelectListItem> GetProductCategoryLookups()
        {
            List<SelectListItem> lists = new List<SelectListItem>();
            // TO DO: Use Map function
            lists = ConvertToListItem(LookupsRepo.GetProductCategoryLookup());
            return lists;
        }

        public List<SelectListItem> GetVendorLookups()
        {
            List<SelectListItem> lists = new List<SelectListItem>();
            // TO DO: Use Map function
            lists = ConvertToListItem(LookupsRepo.GetVendorLookup());
            return lists;
        }

        private List<SelectListItem> ConvertToListItem(List<LookupDTO> list)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            foreach (var item in list)
            {
                result.Add(new SelectListItem { Text = item.Title, Value = item.ID.ToString() });
            }
            return result;
        }

        public bool SendEmail(Models.Vendor model)
        {
            string smtpHost = (ConfigurationManager.AppSettings["SMTPHost"] != null)
          ? ConfigurationManager.AppSettings["SMTPHost"].ToString() : "";

            string consumerEmail = (ConfigurationManager.AppSettings["consumerEmail"] != null)
          ? ConfigurationManager.AppSettings["consumerEmail"].ToString() : "";

            string corporateEmail = (ConfigurationManager.AppSettings["corporateEmail"] != null)
          ? ConfigurationManager.AppSettings["corporateEmail"].ToString() : "";

            string body = String.Empty;
            string subject = string.Format("Case Submitted: Vendor: {0} - Order Number: {1}", model.VendorName, model.OrderNumber);
            string franEmail = model.EmailAddress;

            body = CreateBody(model);
            List<Data.FileStream> attachedImages = CreateAttachedImage(model.VendorLines);


            SmtpClient smtp = new SmtpClient(smtpHost);

            //----------------
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(corporateEmail);
            msg.To.Add(new MailAddress(consumerEmail));

            if (!string.IsNullOrEmpty(franEmail))
                msg.CC.Add(new MailAddress(franEmail));

            msg.Subject = subject;
            msg.Body = body;
            //----------------
            //MailMessage msg_fran = new MailMessage();
            //msg_fran.From = new MailAddress(corporateEmail);
            //msg_fran.To.Add(new MailAddress(franEmail));
            //msg_fran.Subject = subject;
            //msg_fran.Body = body;

            foreach (var item in attachedImages)
            {
                string base64String = Convert.ToBase64String(item.Stream, 0, item.Stream.Length);
                Byte[] bitmapData = Convert.FromBase64String(FixBase64ForImage(base64String));
                Attachment attachment = new Attachment(new MemoryStream(bitmapData), item.FileName, MediaTypeNames.Image.Jpeg);
                attachment.TransferEncoding = TransferEncoding.Base64;
                msg.Attachments.Add(attachment);
                //  msg_fran.Attachments.Add(attachment);
            }

            try
            {
                //---------Send to company
                smtp.Send(msg);

                //---------Send to Franchisee
                // msg.To.Add(new MailAddress(franEmail));
                //  smtp.Send(msg_fran);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
            finally
            {
                smtp.Dispose();
            }

            return true;
        }

        private string FixBase64ForImage(string Image)
        {
            System.Text.StringBuilder sbText = new System.Text.StringBuilder(Image, Image.Length);
            sbText.Replace("\r\n", string.Empty); sbText.Replace(" ", string.Empty);
            return sbText.ToString();
        }

        private string CreateBody(Models.Vendor model)
        {
            string strReturn = string.Empty;
            int counter = 0;

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.AppendLine();
            sb.AppendLine("The following case was submitted with HFC:");
            sb.AppendLine("Incident Date:  " + model.IncidentDate);
            sb.AppendLine("Vendor:  " + model.VendorName);
            sb.AppendLine("Vendor Order Number:  " + model.OrderNumber);
            sb.AppendLine();

            foreach (var line in model.VendorLines)
            {
                counter = counter + 1;
                sb.AppendLine("Issue " + counter + ":");
                sb.AppendLine("Vendor ID Line Number:  " + (String.IsNullOrEmpty(line.LineNumber) ? "-----" : line.LineNumber));
                sb.AppendLine("Product Category:  " + (String.IsNullOrEmpty(line.ProductCategory) ? "-----" : line.ProductCategory));
                sb.AppendLine("Description:  " + (String.IsNullOrEmpty(line.Description) ? "-----" : line.Description));
                sb.AppendLine("Issue Type:  " + (String.IsNullOrEmpty(line.IssueType) ? "-----" : line.IssueType));
                sb.AppendLine("Previous Issues:  " + (String.IsNullOrEmpty(line.IsRemake) ? "-----" : line.IsRemake));
                sb.AppendLine("Remake Order Number:  " + (String.IsNullOrEmpty(line.RemakeOrderNumber) ? "-----" : line.RemakeOrderNumber));

                if (line.IssueType.ToLower() == "backorder")
                {
                    sb.AppendLine("Notification Date:  " + ((line.IssueNotifyDate == null) ? "-----" : line.IssueNotifyDate.ToString()));
                    sb.AppendLine("Reason for delay:  " + (String.IsNullOrEmpty(line.IssueReason) ? "-----" : line.IssueReason));
                    sb.AppendLine("Orig. Estimated Ship Date:  " + ((line.OrigEstShipDate == null) ? "-----" : line.OrigEstShipDate.ToString()));
                    sb.AppendLine("Revised Est. Ship Date:  " + ((line.RevEstShipDate == null) ? "-----" : line.RevEstShipDate.ToString()));
                }
                else
                {
                    sb.AppendLine("Notification Date:  " + "-----");
                    sb.AppendLine("Reason for delay:  " + "-----");
                    sb.AppendLine("Orig. Estimated Ship Date:  " + "-----");
                    sb.AppendLine("Revised Est. Ship Date:  " + "-----");

                }

                sb.AppendLine("");
            }

            strReturn = sb.ToString();
            return strReturn;
        }
        private List<Data.FileStream> CreateAttachedImage(List<Models.VendorLine> list)
        {
            List<Data.FileStream> results = new List<Data.FileStream>();

            if (list != null && list.Count > 0)
                foreach (var item in list)
                {
                    if (item.Images != null && item.Images.Count > 0)
                        foreach (var img in item.Images)
                        {
                            Data.FileStream result = new Data.FileStream()
                            {
                                FileName = item.LineNumber + "_" + img.FileName,
                                Stream = img.Stream
                            };

                            results.Add(result);
                        }
                }

            return results;
        }
    }
}