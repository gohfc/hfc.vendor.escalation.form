﻿using HFC.Vendor.Escalation.Form.DTO;
using HFC.Vendor.Escalation.Form.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.Vendor.Escalation.Form.Services
{
    public interface IAccountService
    {
        AuthenticatedUser Login(LoginModel model);
    }
}
