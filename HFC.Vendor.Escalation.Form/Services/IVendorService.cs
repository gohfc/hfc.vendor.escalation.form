﻿using HFC.Vendor.Escalation.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HFC.Vendor.Escalation.Form.Services
{
    public interface IVendorService
    {
        bool Vendor(Models.Vendor model);
        List<SelectListItem> GetProductCategoryLookups();
        List<SelectListItem> GetVendorLookups();
        bool SendEmail(Models.Vendor model);
    }
}
