﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.Protocols;
using System.Net;
using System.DirectoryServices;
using System.Text;

namespace HFC.Vendor.Escalation.Form.Services
{
    public class AccountService : IAccountService
    {
        public DTO.AuthenticatedUser Login(Models.LoginModel model)
        {
            DTO.AuthenticatedUser user = null;
            try
            {
                DirectoryEntry entry = new DirectoryEntry("LDAP://bbi.corp", model.UserEmail, model.Password);
                Object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = string.Format("(&(objectClass=user)(objectCategory=person)(proxyaddresses=SMTP:{0}))", model.UserEmail);
                SearchResult sr = search.FindOne();
                if (sr != null)
                {
                    System.DirectoryServices.DirectoryEntry de = sr.GetDirectoryEntry();
                    var userAccountControlValue = 0;
                    int.TryParse(de.Properties["UserAccountControl"].Value.ToString(), out userAccountControlValue);
                    var isAccountDisabled = Convert.ToBoolean(userAccountControlValue & 0x0002);
                    var isNormalAccount = Convert.ToBoolean(userAccountControlValue & 0x0200);
                    if (!isAccountDisabled && de.Properties["sAMAccountName"].Value != null && de.Properties["userAccountControl"].Value != null)
                    {
                        string firstName = de.Properties["givenName"].Value != null ? (string)de.Properties["givenName"].Value : "";
                        string email = de.Properties["userPrincipalName"].Value != null ? (string)de.Properties["userPrincipalName"].Value : "";
                        string lastName = de.Properties["sn"].Value != null ? (string)de.Properties["sn"].Value : "";
                        string ownerNumber = de.Properties["physicalDeliveryOfficeName"].Value != null ? (string)de.Properties["physicalDeliveryOfficeName"].Value : "";
                        user = new DTO.AuthenticatedUser
                        {
                            FirstName = firstName,
                            LastName = lastName,
                            EmailAddress = email,
                            OwnerNumber = ownerNumber
                        };
                    }
                }
            }
            catch
            {
            }
            return user;
        }
    }
}