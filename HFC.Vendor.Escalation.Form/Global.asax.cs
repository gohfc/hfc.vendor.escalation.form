﻿//using HFC.Vendor.Escalation.Form.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using log4net;
using System.Reflection;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml;

using Gelf4Net.Appender;
using Gelf4Net.Layout;

using log4net;
using log4net.Repository.Hierarchy;
//using Unity;
using System.IO;
namespace HFC.Vendor.Escalation.Form
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private const string Log4NetUrlCustomPropertyName = "url";
        private const string Log4NetHostCustomPropertyName = "host";
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/log4net.config"))); // must have this line
            ConfigureLog4Net();
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            //UnityConfig.RegisterTypes();
            //log.Error("this is a tets for vendor escalation!");
        }

        private void ConfigureLog4Net()
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository();
            foreach (var appender in hierarchy.Root.Appenders.OfType<AsyncGelfUdpAppender>())

            {
                var layout = appender.Layout as GelfLayout;
                if (layout != null)
                {
                    var siteName = HostingEnvironment.SiteName;
                    var assembly = Assembly.GetExecutingAssembly();
                    var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                    var version = fvi.FileVersion;
                    layout.AdditionalFields = string.Format(
                        "Host:%property{{{3}}},Version:{0},Level:%level,SiteName:{1},Url:%property{{{2}}}",
                        version, siteName, Log4NetUrlCustomPropertyName, Log4NetHostCustomPropertyName);
                }

                appender.ActivateOptions();
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

            LogicalThreadContext.Properties[Log4NetUrlCustomPropertyName] =
                HttpContext.Current.Request.Url.AbsoluteUri;
            LogicalThreadContext.Properties[Log4NetHostCustomPropertyName] =
                HttpContext.Current.Request.Url.Host;

        }
        /// <summary>
        ///     Manage error on web site
        /// </summary>
        protected void Application_Error()
        {
            var ex = Server.GetLastError();
            if (ex != null)
                log.Error("Unhandled exception",
                    ex);
        }

    }
}