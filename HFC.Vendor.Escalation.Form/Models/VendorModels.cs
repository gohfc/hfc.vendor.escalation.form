﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HFC.Vendor.Escalation.Form.Models
{
    [Table("Vendor")]
    public class Vendor
    {
        [HiddenInputAttribute]
        public string FirstName { get; set; }

        [HiddenInputAttribute]
        public string LastName { get; set; }

        [HiddenInputAttribute]
        public string EmailAddress { get; set; }

        [HiddenInputAttribute]
        public string OwnerNumber { get; set; }

        [HiddenInputAttribute]
        public string SubmitControl { get; set; }

        //[HiddenInputAttribute]
        public Int32 LockedVendorId { get; set; }

        //[HiddenInputAttribute]
        public string LockedOrderNumber { get; set; }


        [Display(Name = "Owner Name")]
        public String OwnerName { get; set; }
        
        [Display(Name = "Terr Name")]
        public String TerrName { get; set; }

        [Required]
        [Display(Name = "Incident Date*")]
       // [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? IncidentDate { get; set; }

        [Required]
        [Display(Name = "Original Order Date*")]
       // [DataType(DataType.Date),DisplayFormat(DataFormatString = "{MM/dd/yyyy}")]
        public DateTime? OroginalOrderDate { get; set; }

        [Required]
        [Display(Name = "Who is the Vendor?*")]
        public Int32 VendorLookupID { get; set; }

        [HiddenInputAttribute]
        public string VendorName { get; set; }

        public List<SelectListItem> VendorLookups { get; set; }

        [Display(Name = "What is the Vendor Order Number?*")]
        [StringLength(50, ErrorMessage = "The {0} max length is {1} long.")]
        public String OrderNumber { get; set; }

        public List<string> Images { get; set; }

        public List<VendorLine> VendorLines { get; set; }
    }

    [Table("VendorLookup")]
    public class VendorLookup
    {
        public int ID { get; set; }
        public String Title { get; set; }
    }

    [Table("ProductCategoryLookup")]
    public class ProductCategoryLookup
    {
        public int ID { get; set; }
        public String Title { get; set; }
    }

    [Serializable]
    public class FileStream
    {
        public String VLine { get; set; }
        public String FileName { get; set; }
        public String FileType { get; set; }
        public byte[] Stream { get; set; }
    }

    public class VendorLine
    {
        // Hidden properties
        public int ProductCategoryId { get; set; }
        // Display properties
        public String VendorID { get; set; }
        public String LineNumber { get; set; }
        public String ProductCategory { get; set; }
        public String IsRemake { get; set; }
        public String RemakeOrderNumber { get; set; }
        //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? IssueNotifyDate { get; set; }
        //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? OrigEstShipDate { get; set; }
        //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? RevEstShipDate { get; set; }
        public String IssueReason { get; set; }
        public String IssueType { get; set; }
        public String Description { get; set; }
        public List<FileStream> Images { get; set; }
        [Display(Name = "Images")]
        public string FileNames 
        {
            get 
            {
                if (this.Images != null && this.Images.Count > 0)
                {
                    string fileNames = string.Empty;
                    for (int i = 1; i <= this.Images.Count; i++)
                    {
                        string fileName = this.Images[i - 1].FileName;
                        string sprtr = (i < this.Images.Count) ? ", " : "";
                        fileNames += fileName + sprtr;
                    }
                    return fileNames;
                }
                else
                    return null;
            }
        }
    }

    public class VendorLines
    {
        // Hidden properties
        [HiddenInputAttribute]
        public string SubmitControl { get; set; }

        //[Required]
        [Display(Name = "1. Unique Vendor ID Line Number*")]
        public String LineNumber { get; set; }

        //[Required]
        [Display(Name = "2. Which Product Category is this?*")]
        public int CategoryLookupID { get; set; }
        public List<SelectListItem> ProductCategoryLookups { get; set; }
        
        //[Required]
        [Display(Name = "3. Description of the issue for this line*")]
        public String Description { get; set; }

        //[Required]
        [Display(Name = "4. Issue Type*")]
        public String IssueType { get; set; }
        public List<SelectListItem> IssueTypes { get; set; }

        //[Required]
        [Display(Name = "5. Has there been a previous remake on this order?")]
        public bool IsRemake { get; set; }
        public List<SelectListItem> IsRemakeOptions { get; set; }

        //[Required]
        [Display(Name = "Please list All the remake order numbers(current and previous)")]
        public String RemakeOrderNumber { get; set; }
        
        //[Required]
        [Display(Name = "Notification Date")]
       // [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? IssueNotifyDate { get; set; }

        //[Required]
        [Display(Name = "Orig. Estimated Ship Date")]
       // [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? OrigEstShipDate { get; set; }

        //[Required]
        [Display(Name = "Revised Est. Ship Date")]
        //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? RevEstShipDate { get; set; }

        //[Required]
        [Display(Name = "Reason for delay*")]
        public String IssueReason { get; set; }

        public List<VendorLine> ProductLines { get; set; }
    }
}