﻿using HFC.Vendor.Escalation.Form.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using System.Reflection;


namespace HFC.Vendor.Escalation.Form.Controllers
{
    //[Authorize]
    public class VendorController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private IVendorService _vendorService;

        public VendorController()
        {
            _vendorService = new VendorService();
        }

        [HttpGet]
        public ActionResult Vendor()
        {
            if (string.IsNullOrEmpty(Request["firstName"])
                | string.IsNullOrEmpty(Request["lastName"])
                | string.IsNullOrEmpty(Request["emailAddress"])
                | string.IsNullOrEmpty(Request["ownerNumber"]))
            {
                return RedirectToAction("Login", "Account");
            }

            return View(InitializeVendor(null));
        }

        [HttpPost]
        public ActionResult Vendor(Models.Vendor model)
        {
			log.InfoFormat("Model submitted {0}", Newtonsoft.Json.JsonConvert.SerializeObject(model));

            if (model.SubmitControl == "CancelVendor")
            {
                Session.Remove("Vendor");
                Session.Remove("VendorLines");
                return View(InitializeVendor(null));
            }
            if (model.SubmitControl != "Vendor")
            {
                Session["Vendor"] = model;
                return View(InitializeVendor(model));
            }
            var vendorLines = Session["VendorLines"] as Models.VendorLines;
            if (vendorLines == null)
            {
                vendorLines = new Models.VendorLines
                {
                    ProductLines = new List<Models.VendorLine>()
                };
            }

            if(string.IsNullOrEmpty(model.FirstName) 
               | string.IsNullOrEmpty(model.LastName)
               | string.IsNullOrEmpty(model.EmailAddress)
               | string.IsNullOrEmpty(model.OwnerNumber))
            {
                return RedirectToAction("Login", "Account");
            }

            model.VendorLines = vendorLines.ProductLines;
            model.VendorLookupID = model.LockedVendorId;
            model.OrderNumber = model.LockedOrderNumber;
            model.VendorName = model.VendorLookupID != 0 ? GetVendorLookups().Find(v => v.Value == model.LockedVendorId.ToString()).Text : string.Empty;
            
            var result = _vendorService.Vendor(model);
            if (result)
            {
                //-----Send Email
				if (model.VendorLines.Count != 0 )
					_vendorService.SendEmail(model);
                //-------------------

                Session.Remove("Vendor");
                Session.Remove("VendorLines");
                ModelState.Clear();
                return View(InitializeVendor(null));
            }
            else
            {
                return View(InitializeVendor(model));
            }
        }

        [HttpGet]
        public ActionResult VendorLines()
        {
            var vendorLines = Session["VendorLines"] as Models.VendorLines;
            return View(InitializeVendorLines(vendorLines));
        }

        [HttpPost]
        public ActionResult VendorLines([System.Web.Http.FromBody] Models.VendorLines model)
        {
            if (model != null)
            {
                if (model.SubmitControl != "VendorLines")
                {
                    return View(InitializeVendorLines(model, true));
                }

                Models.VendorLine line = new Models.VendorLine
                {
                    Description = model.Description,
                    IsRemake = model.IsRemake ? "Yes" : "No",
                    IssueNotifyDate = model.IssueNotifyDate != default(DateTime) ? model.IssueNotifyDate : DateTime.Now,
                    OrigEstShipDate = model.OrigEstShipDate != default(DateTime) ? model.OrigEstShipDate : DateTime.Now,
                    RevEstShipDate = model.RevEstShipDate != default(DateTime) ? model.RevEstShipDate : DateTime.Now,
                    IssueReason = model.IssueReason,
                    IssueType = model.IssueType,
                    LineNumber = model.LineNumber,
                    RemakeOrderNumber = model.RemakeOrderNumber,
                    ProductCategoryId = model.CategoryLookupID,
                    VendorID = Session["Vendor"] as Models.Vendor != null ? (Session["Vendor"] as Models.Vendor).VendorLookupID.ToString() : null
                };
                var vendorLines = Session["VendorLines"] as Models.VendorLines;
                if (vendorLines == null)
                {
                    vendorLines = new Models.VendorLines
                    {
                        ProductLines = new List<Models.VendorLine>()
                    };
                }

                List<Models.FileStream> files = Fnc.Vendor.ReadImages(Request.Files);
                if (files != null && files.Count > 0)
                {
                    line.Images = new List<Models.FileStream>();
                    for (int i = 0; i < files.Count; i++)
                    {
                        var fileStream = new Models.FileStream();
                        fileStream.FileName = files[i].FileName;
                        fileStream.FileType = files[i].FileType;
                        fileStream.Stream = files[i].Stream;
                        fileStream.VLine = line.LineNumber;
                        line.Images.Add(fileStream);

                    }
                }
                vendorLines.ProductLines.Add(line);
                model.ProductLines = vendorLines.ProductLines;
                Session["VendorLines"] = vendorLines;
            }
            ModelState.Clear();
            return View(InitializeVendorLines(model, true));
        }

        [HttpGet]
        public ActionResult Copy(string id)
        {
            var vendorLines = Session["VendorLines"] as Models.VendorLines;
            var vendor = Session["Vendor"] as Models.Vendor;
            var copy = Fnc.Vendor.CopyVendorLine(vendorLines.ProductLines, id);
            
            var model = InitializeVendorLines(vendorLines);
            model.ProductLines = vendorLines.ProductLines;
            model.CategoryLookupID = copy.ProductCategoryId;
            model.Description = copy.Description;
            model.IsRemake = copy.IsRemake == "Yes" ? true : false;
            model.IssueNotifyDate = copy.IssueNotifyDate;
            model.OrigEstShipDate = copy.OrigEstShipDate;
            model.RevEstShipDate = copy.RevEstShipDate;
            model.IssueReason = copy.IssueReason;
            model.IssueType = copy.IssueType;
            model.RemakeOrderNumber = copy.RemakeOrderNumber;
            Session["VendorLines"] = model;
            vendor.VendorLines = model.ProductLines;

            return View("Vendor", InitializeVendor(vendor));
          
        }

        public ActionResult Delete(string id)
        {
            var vendorLines = Session["VendorLines"] as Models.VendorLines;
            var vendor = Session["Vendor"] as Models.Vendor;
            vendorLines.ProductLines = Fnc.Vendor.DeleteVendorLine(vendorLines.ProductLines, id);
            vendorLines.IsRemake = false;
            vendorLines.IssueNotifyDate = DateTime.Now;
            vendorLines.IssueReason = null;
            vendorLines.IssueType = null;
            vendorLines.LineNumber = null;
            vendorLines.OrigEstShipDate = DateTime.Now;
            vendorLines.RemakeOrderNumber = null;
            vendorLines.RevEstShipDate = DateTime.Now;
            Session["VendorLines"] = vendorLines;

            vendor.VendorLines = vendorLines.ProductLines;
            var model = InitializeVendor(vendor);
            model.VendorLines = vendorLines.ProductLines;

            return View("Vendor", model);
        }

        private Models.Vendor InitializeVendor(Models.Vendor model)
        {
            if (model == null)
                model = new Models.Vendor();

            if (model.VendorLookupID != 0)
                model.LockedVendorId = model.VendorLookupID;
            else if (model.LockedVendorId != 0)
                model.VendorLookupID = model.LockedVendorId;

            if (!string.IsNullOrEmpty(model.OrderNumber))
                model.LockedOrderNumber = model.OrderNumber;
            else if (!string.IsNullOrEmpty(model.LockedOrderNumber))
                model.OrderNumber = model.LockedOrderNumber;

             model.IncidentDate = model.IncidentDate != default(DateTime) ? model.IncidentDate : DateTime.Now; 
             model.OroginalOrderDate = model.OroginalOrderDate != default(DateTime) ? model.OroginalOrderDate : DateTime.Now;


            var ownername = model.OwnerName;
            var terrname = model.TerrName;


            model.VendorLookups = GetVendorLookups();
            if (model.VendorLookups[0].Value != "0")
                model.VendorLookups.Insert(0, new SelectListItem { Selected = true, Text = "Please Select Vendor", Value = "0" });
            model.VendorLookups.Find(i => i.Value == model.VendorLookupID.ToString()).Selected = true;
            model.FirstName = Request["firstName"] != null ? Request["firstName"].ToString() : model.FirstName;
            model.LastName = Request["lastName"] != null ? Request["lastName"].ToString() : model.LastName;
            model.EmailAddress = Request["emailAddress"] != null ? Request["emailAddress"].ToString() : model.EmailAddress;
            model.OwnerNumber = Request["ownerNumber"] != null ? Request["ownerNumber"].ToString() : model.OwnerNumber;
            model.TerrName = Request["terrName"] != null ? Request["terrName"].ToString() : model.TerrName;
            model.OwnerName = Request["ownerName"] != null ? Request["ownerName"].ToString() : model.OwnerName;
            model.OwnerName = ownername;
            model.TerrName = terrname;
            return model;
        }

        private Models.VendorLines InitializeVendorLines(Models.VendorLines model, bool cleanLine = false)
        {
            if (model == null)
                model = new Models.VendorLines();

            model.IssueTypes = new List<SelectListItem>();
            model.IssueTypes.Add(new SelectListItem { Selected = false, Text = "Back Orders", Value = "Backorder" });
            model.IssueTypes.Add(new SelectListItem { Selected = false, Text = "Feedback", Value = "Feedback" });
            model.IssueTypes.Add(new SelectListItem { Selected = false, Text = "Other", Value = "Other" });
            model.IssueTypes.Add(new SelectListItem { Selected = false, Text = "Parts", Value = "Parts" });
            model.IssueTypes.Add(new SelectListItem { Selected = false, Text = "Remake", Value = "Remake" });
            model.IssueTypes.Add(new SelectListItem { Selected = false, Text = "Repair", Value = "Repair" });
            
            
            if (model.IssueTypes[0].Value != "None")
                model.IssueTypes.Insert(0, new SelectListItem { Selected = true, Text = "Please Select Issue Type", Value = "None" });

            model.IsRemakeOptions = new List<SelectListItem>();
            model.IsRemakeOptions.Add(new SelectListItem { Selected = true, Text = "Yes", Value = "true" });
            model.IsRemakeOptions.Add(new SelectListItem { Selected = false, Text = "No", Value = "false" });

            model.ProductCategoryLookups = GetProductCategoryLookups();
            if (model.ProductCategoryLookups[0].Value != "0")
                model.ProductCategoryLookups.Insert(0, new SelectListItem { Selected = true, Text = "Please Select Product Category", Value = "0" });
            model.ProductLines = model.ProductLines == null || model.ProductLines.Count == 0 ? new List<Models.VendorLine>() : model.ProductLines;
            var category = model.ProductLines.Find(l => l.ProductCategoryId == model.CategoryLookupID && string.IsNullOrEmpty(l.ProductCategory));
            if (category != null)
                category.ProductCategory = model.ProductCategoryLookups.Find(x => x.Value == model.CategoryLookupID.ToString()).Text;
            model.IssueNotifyDate = model.IssueNotifyDate != default(DateTime) ? model.IssueNotifyDate : DateTime.Now;
            model.OrigEstShipDate = model.OrigEstShipDate != default(DateTime) ? model.OrigEstShipDate : DateTime.Now;
            model.RevEstShipDate = model.RevEstShipDate != default(DateTime) ? model.RevEstShipDate : DateTime.Now;
            if (cleanLine)
            {
                model.IssueReason = null;
                model.IssueType = null;
                model.LineNumber = null;
                model.RemakeOrderNumber = null;
                model.CategoryLookupID = 0;
                model.Description = null;
                model.IsRemake = default(bool);
            }

            return model;
        }

        private List<SelectListItem> GetVendorLookups()
        {
            var lookups = Session["VendorLookups"] as List<SelectListItem>;
            if (lookups == null || lookups.Count == 0)
            {
                lookups = this._vendorService.GetVendorLookups();
                Session["VendorLookups"] = lookups;
            }
            return lookups;
        }

        private List<SelectListItem> GetProductCategoryLookups()
        {
            var lookups = Session["ProductCategoryLookups"] as List<SelectListItem>;
            if (lookups == null || lookups.Count == 0)
            {
                lookups = this._vendorService.GetProductCategoryLookups();
                Session["ProductCategoryLookups"] = lookups;
            }
            return lookups;
        }
    }
}
