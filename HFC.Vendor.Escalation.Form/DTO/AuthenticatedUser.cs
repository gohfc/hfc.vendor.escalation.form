﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HFC.Vendor.Escalation.Form.DTO
{
    public class AuthenticatedUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string OwnerNumber { get; set; }
    }
}